package com.denis.fomin.geoquiz

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.denis.fomin.geoquiz.ui.theme.GeoQuizTheme
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.filter


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            GeoQuizTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    QuestionScreen(context = this)
                }
            }
        }
    }
}

@Composable
fun QuestionScreen(context: ComponentActivity, viewModel: MainViewModel = viewModel()) {
    val questionsState by viewModel.questions.collectAsStateWithLifecycle()
    var selectedQuestionIdx by rememberSaveable { mutableStateOf(0) }

    val questions = questionsState.filter { it.userAnswer == null }

    if (selectedQuestionIdx < 0) {
        selectedQuestionIdx = questions.size - 1
    } else if (selectedQuestionIdx > questions.size - 1) {
        selectedQuestionIdx = 0
    }

    if (questions.isEmpty()) {
        ResultElement(viewModel = viewModel)
    } else {
        val question = questions[selectedQuestionIdx]
        val title = stringResource(question.textResId)

        val onAnswerClick = {
                answer: Boolean ->

            val toastText = if (question.answer != answer) {
                R.string.incorrect_toast
            } else {
                R.string.correct_toast
            }

            Toast.makeText(
                context,
                toastText,
                Toast.LENGTH_SHORT,
            ).show()

            viewModel.setUserAnswer(question.uuid, answer)
        }

        val modifySelectedQuestionIdx = {
                mod: Int ->
            selectedQuestionIdx += mod
        }

        QuestionElement(
            title = title,
            onAnswerClick = onAnswerClick,
            onNavClick = modifySelectedQuestionIdx,
        )
    }

}

@Composable
fun ResultElement(viewModel: MainViewModel) {
    val questionsState by viewModel.questions.collectAsStateWithLifecycle()
    val correctCount = questionsState.count { it.userAnswer == it.answer }

    Column(
        modifier = Modifier.padding(40.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = "THANKS! YOUR RESULT:",
            fontSize = 28.sp,
            fontWeight = FontWeight.Bold,
        )
        Text(
            text = "$correctCount / ${questionsState.count()}",
            fontSize = 50.sp,
            fontWeight = FontWeight.Bold,
            color = Color.Green,
            modifier = Modifier.padding(top = 40.dp),
        )
        Spacer(
            modifier = Modifier.fillMaxHeight()
                .weight(1f)
        )
        Button(
            onClick = { viewModel.reset() },
            modifier = Modifier.fillMaxWidth(),
        ) {
            Text(text = "RESET")
        }
    }
}

@Composable
fun QuestionElement(
    title: String,
    onAnswerClick: (userAnswer: Boolean) -> Unit,
    onNavClick: (mod: Int) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        Text(
            text = title,
            modifier = modifier,
        )

        val spacingModifier = Modifier.padding(10.dp)
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = spacingModifier
            ) {
                Button(
                    onClick = { onAnswerClick(true) },
                    modifier = spacingModifier
                ) {
                    Text(
                        text = "True",
                    )
                }

                Button(
                    onClick = { onAnswerClick(false) },
                    modifier = spacingModifier
                ) {
                    Text(
                        text = "False",
                    )
                }
            }
            Row{
                Button(
                    onClick = { onNavClick(-1) },
                    modifier = spacingModifier
                ) {
                    Text(
                        text = "<",
                    )
                }

                Button(
                    onClick = { onNavClick(1) },
                    modifier = spacingModifier
                ) {
                    Text(
                        text = ">",
                    )
                }
            }
        }
    }
}


// Dumb composable for the sake of preview
@Preview(
    showBackground = true,
    showSystemUi = true,
)
@Composable
fun GeoQuizApp() {
    QuestionElement(
        title = "Some long long long long question to display",
        onAnswerClick = {
            _: Boolean ->
        },
        onNavClick = {
            _: Int ->
        }
    )
}