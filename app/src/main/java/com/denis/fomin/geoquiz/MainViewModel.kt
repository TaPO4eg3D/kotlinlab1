package com.denis.fomin.geoquiz

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import java.util.UUID

data class Question(
    val uuid: UUID,
    val textResId: Int,
    val answer: Boolean,
    val userAnswer: Boolean? = null,
)

class MainViewModel: ViewModel() {
    // Expose screen UI state
    private val _questions = MutableStateFlow(listOf(
        Question(
            uuid = UUID.randomUUID(),
            textResId = R.string.question_oceans,
            answer = false,
        ),
        Question(
            uuid = UUID.randomUUID(),
            textResId = R.string.question_africa,
            answer = false,
        )
    ))
    val questions: StateFlow<List<Question>> = _questions.asStateFlow()

    fun setUserAnswer(uuid: UUID, answer: Boolean) {
        _questions.update { currentState ->
            currentState.map {
                if (it.uuid == uuid) {
                    it.copy(
                        userAnswer = answer,
                    )
                } else {
                    it.copy()
                }
            }
        }
    }

    fun reset() {
        _questions.update { currentState ->
            currentState.map { it.copy(userAnswer = null) }
        }
    }
}